c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Assignment #5 Requirements:

*Three Parts:*

1. Create server-side validation of Pet Store process.
2. Create table for Pet Store data.
3. Provide screenshots of 'index.php' and 'add_petstore_process.php'(error page).

#### README.md file should include the following items:

* Screenshot of 'index.php' for A5;
* Screenshot of error page for 'add_petstore_process.php';
* Link to local host LIS4381 web app.


#### Assignment Screenshots:

*Screenshot of 'index.php':

![Index](img/index.png)

*Screenshot of 'add_petstore_php':

![Error](img/error.png)


Link to LIS4381 web app: 
http://localhost/lis4381_test/a5/index.php
