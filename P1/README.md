c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Project 1 Requirements:

1. Create a mobile app in Android Studio for a virtual business card.


#### README.md file should include the following items:

* Screenshot of running applications first user interface;
* Screenshot of running applications second user interface;



#### Assignment Screenshots:

*Screenshot of first user interface:

![First user interface](img/businesscard.png)

*Screenshot of second user interface:

![Second user interface](img/details.png)

