c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Assignment # Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation;
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git comments w/ short descriptions 
* Bitbucket repo links: a) This assignment and b) the completed tutorials above (bitbucketstationslocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init -  change an existing directory in a Git-enabled repository
2. git status - show what additions/changes are going to be made with next commit
3. git add - add the files as it currently stands to your next commit
4. git commit - send out your staged commit to your repository
5. git push - upload local content to a remote repository
6. git pull - update your local cloned repository with the latest version
7. git log - show all past commits and what has moved

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/hrs14c/bitbucketstationslocations/src/master/)
