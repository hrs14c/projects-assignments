import java.util.Scanner;
import java.util.Random; 


class RandomArray
{
    public static void main (String args[])
    {
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println();



        Scanner sc = new Scanner(System.in);
        Random ran = new Random();
        int arraySize = 0;
        int i = 0;

        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        arraySize = sc.nextInt();

        int myArray[] = new int[arraySize];

        System.out.println("for loop:");
        for (i = 0; i < myArray.length; i++)
            {
                System.out.println(ran.nextInt());
            }

        System.out.println("\nEnhanced for loop:");
        for (int n: myArray)
            {
                System.out.println(ran.nextInt());
            }

        System.out.println("\nwhile loop:");
        i = 0;
        while (i < myArray.length)
            {
                System.out.println(ran.nextInt());
                i++;
            }

        i = 0;
        System.out.println("\ndo...while loop:");
        do
            {
                System.out.println(ran.nextInt());
                i++;
            }
        while (i < myArray.length);
    }


}