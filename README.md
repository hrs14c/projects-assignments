﻿> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

1. [A1 README.md](A1/README.md "My A1 READMEmd file")	
	
	
	• Install AMPPS 
	
	• Install JDK
	
	• Install Android Studio and create My First App
	
	• Provide screenshots of installations
	
	• Create Bitbucket repo
	
	• Complete Bitbucket tutorials (bitbucketstationslocations)
	
	• Provide git command descriptions 
	
2. [A2 README.md](A2/README.md "My A2 READMEmd file")
	
	• Create Healthy Recipes Android app
	
	• Provide screenshots of completed app 
	
3. [A3 README.md](A3/README.md "My A3 READMEmd file")

	• Create an ERD in MySQL and input at least 10 data items for each table.
	
	• Create My Event mobile app in Android Studio.
	
	• Provide screenshots for both.
	
4. [P1 README.md](P1/README.md "My P1 READMEmd file")

	• Create a mobile app business card.
	
	
5. [A5 README.md](A5/README.md "My A5 READMEmd file")

	• Create server-side validation of Pet Store process.

	• Create table for Pet Store data.

	• Provide screenshots of 'index.php' and 'add_petstore_process.php'(error page).
	
6. [P2 README.md](P2/README.md "My P2 READMEmd file")

	• Add 'delete' function to A5 pet store table.

	• Add 'edit' function to A5 pet store table.
	
	• Provide indicated screenshots.