-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB
COMMENT = '	';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mydb`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`customer` (
  `cus_id` SMALLINT UNSIGNED NULL,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mydb`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) ,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) ,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `mydb`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `mydb`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, '101 Dog Road', 'Dogs R Us', 'Chicago', 'IL', 34749, 9045678276, 'dogrus@gmail.com', 'dogsrus.com', 100, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, '303 Cat Street', 'Cats R Us', 'Chicago', 'IL', 34729, 7392736456, 'catsrus@gmail.com', 'catsrus.com', 200, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, '567 Animal Lane', 'Tallahassee Animal Shelter', 'Tallahassee', 'FL', 32304, 8506787263, 'tallyanimals@yahoo.com', 'tallyanimals.com', 300, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, '78 Cobblestone Ln', 'Pet Supermarket', 'St. Augustine', 'FL', 32080, 9045406789, 'petsupermarket@aol.com', 'petsupermarket.com', 400, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, '123 Main Street', 'Furry Friends', 'Orlando', 'FL', 43456, 6708498753, 'furryfriends@gmail.com', 'furryfriends.com', 500, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, '789 High Road', 'Pet Palace', 'Tallahassee', 'FL', 32304, 8505673847, 'petpalace@gmail.com', 'petpalace.com', 600, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, '32 B Street', 'Furever Friends', 'Jacksonville', 'FL', 32890, 9045672346, 'fureverfriends@yahoo.com', 'fureverfriends.com', 500, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, '90 Tulane Avenue', 'Carol\'s Critters', 'New Orleans', 'LI', 67995, 6478903467, 'carolscritters@hotmail.com', 'carolscritters.com', 800, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, '100 Low Road', 'Pets Galore', 'Gainesville', 'FL', 56783, 9043787568, 'petsgalore@gmail.com', 'petsgalore.com', 400, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_street`, `pst_name`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, '43 Snake Lane', 'Must Love Dogs', 'Houston', 'TX', 77609, 8995642359, 'mustlovedogs@bellsouth.net', 'mustlovedogs.com', 300, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Heather', 'Sacco', '921 Dent Street', 'Tallahassee', 'FL', 32304, 9045405173, 'heatsacco@gmail.com', 125, 125, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Wyatt ', 'Timmons', '403 Prince Street', 'Tallahassee', 'FL', 32304, 9045405265, 'timmons.wyatt@gmail.com', 350, 350, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Kathe', 'O\'Donnelly', '5765 Rudolph Ave', 'St. Augustine', 'FL', 32080, 9044614674, 'katheodonnelly@bellsouth.net', 0, 570, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Sean', 'Sacco', '43 Mariposa AVe', 'St. Augustine ', 'FL', 32080, 9045013181, 'seansacco2@hotmail.com', 140, 300, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Casey', 'Alvarez', '15 N Miami Ave', 'Miami', 'FL', 30028, 9045405103, 'caseylanealv@gmail.com', 45, 120, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Logan', 'Holanchock', '1 N Trident Ave', 'Orlando', 'FL', 34548, 9045668802, 'lah1078@yahoo.com', 0, 340, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Aly', 'Snowden', '45 America Street', 'Houston', 'TX', 77890, 7497695678, 'alysnow@gmail.com', 90, 540, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Dulcinea', 'Hellings', '7 N 72nd Ave', 'New York', 'NY', 22345, 9043155613, 'dulciegirl@bellsout.net', 300, 300, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Ryan', 'Murray', '12 Brooke Ave', 'Morton', 'PA', 33467, 2346785467, 'rem@gmail.com', 700, 745, NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Chelsea', 'Meric', '34 Venetian Way', 'Tallahassee', 'FL', 32304, 8507893456, 'chelseameric@yahoo.com', 45, 125, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, NULL, 'Border Collie', 'm', 450, 559, 32, 'black/white', '2015-07-02', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, NULL, 'Boxer', 'f', 330, 250, 62, 'tan/white', '2017-04-02', 'y', 'n', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, NULL, 'Pitbull', 'm', 220, 340, 22, 'grey', '2018-01-30', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, NULL, 'Short-hair Tabby', 'f', 150, 250, 78, 'tan', '2017-08-09', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, NULL, 'Weimaraner', 'f', 400, 500, 34, 'grey', '2017-09-15', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, NULL, 'Chinchilla', 'm', 300, 600, 45, 'black/tan/white', '2015-09-14', 'y', 'n', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, NULL, 'African Grey Parrot', 'f', 225, 500, 102, 'grey', '2013-04-12', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, NULL, 'Ferret', 'f', 145, 225, 4, 'white', '2018-04-25', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, NULL, 'Siamese', 'm', 225, 350, 30, 'black/white', '2017-05-18', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, NULL, 'Parakeet', 'm', 80, 125, 3, 'blue/yellow', '2014-12-04', 'y', 'y', NULL);

COMMIT;

