c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Project 2 Requirements:

*Three Parts:*

1. Add 'delete' function to A5 pet store table.
2. Add 'edit' function to A5 pet store table.
3. Provide indicated screenshots.

#### README.md file should include the following items:

* Screenshot of 'index.php' for P2;
* Screenshot of error page for 'edit_petstore_process.php';
* Screenshot of 'edit_petstore.php';
* Screenshot of Carousel;
* Screenshot of RSS Feed;
* Link to local host LIS4381 web app.


#### Assignment Screenshots:

*Screenshot of 'index.php':

![Index](img/index.png)

*Screenshot of 'edit_petstore.php':

![Error](img/edit.png)

*Screenshot of 'edit_petstore_process.php'(error page):

![Error](img/error.png)

*Screenshot of Carousel (home page):

![Error](img/carousel.png)

*Screenshot of RSS Feed:

![Error](img/rss.png)

Link to LIS4381 web app: 
http://localhost/lis4381_test/p2/index.php
