c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Assignment #3 Requirements:

*Three Parts:*

1. Create an ERD in MySQL and input at least 10 data items for each table.
2. Create My Event mobile app in Android Studio.
3. Provide screenshots for both.

#### README.md file should include the following items:

* Screenshot of ERD;
* Screenshot of running applications first user interface;
* Screenshot of running applications second user interface;
* Links to a3.mwb and a3.sql


#### Assignment Screenshots:

*Screenshot of first user interface:

![First user interface](img/firstUI.png)

*Screenshot of second user interface:

![Second user interface](img/secondUI.png)

*Screenshot of ERD:

![ERD](img/ERD.png)

Links: 
(A3/a3MWB.mwb)

(A3/a3SQL.sql)