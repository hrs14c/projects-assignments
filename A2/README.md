c> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Heather Sacco

### Assignment #2 Requirements:

*Two Parts:*

1. Create a mobile recipe app using Android Studio.
2. Provide screenshots of completed app.

#### README.md file should include the following items:

* Screenshot of main activity running;
* Screenshot of recipe activity running.


#### Assignment Screenshots:

*Screenshot of main activity running:

![AMPPS Installation Screenshot](img/main.png)

*Screenshot of recipe activity running:

![JDK Installation Screenshot](img/recipe.png)

